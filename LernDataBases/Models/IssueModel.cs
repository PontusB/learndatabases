﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LernDataBases.Models
{
    class IssueModel
    {
        [BsonId]
        public int IssueID { get; set; }
        public DateTime IssueDate { get; set; }
        public string ShortDescription {get; set;}
        public string LongDescription { get; set; }
        public bool Solved { get; set; }
        public string ErrorCode { get; set; }
    }
}
