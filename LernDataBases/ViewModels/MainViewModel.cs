﻿using LernDataBases.Commands;
using LernDataBases.Models;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LernDataBases.ViewModels
{
    class MainViewModel : BaseViewModel
    {
        private string _currentShortDescription;
        private string _currentLongDescription;
        private string _currentErrorCode;
        private DateTime _currentIssueDate;
        private bool _currentSolved;
        private int _currentIssueID;
        private bool _addingNewIssue;
        private IList<IssueModel> _allIssuesList;
        private IssueModel _selectedIssue;
        private bool _showOnlySolved;
        private bool sortByDescription;

        //Form propertys
        public string CurrentShortDescription
        {
            get => _currentShortDescription;
            set
            {
                if (_currentShortDescription != value)
                {
                    _currentShortDescription = value;
                    OnPropertyChanged(nameof(CurrentShortDescription));
                }
            }
        }
        public string CurrentLongDescription
        {
            get => _currentLongDescription; set
            {
                if (_currentLongDescription != value)
                {
                    _currentLongDescription = value;
                    OnPropertyChanged(nameof(CurrentLongDescription));
                }
            }
        }
        public string CurrentErrorCode
        {
            get => _currentErrorCode; set
            {
                if (_currentErrorCode != value)
                {
                    _currentErrorCode = value;
                    OnPropertyChanged(nameof(CurrentErrorCode));
                }
            }
        }
        public DateTime CurrentIssueDate
        {
            get => _currentIssueDate; set
            {
                if (_currentIssueDate != value)
                {
                    _currentIssueDate = value;
                    OnPropertyChanged(nameof(CurrentIssueDate));
                }
            }
        }
        public bool CurrentSolved
        {
            get => _currentSolved; set
            {
                if (_currentSolved != value)
                {
                    _currentSolved = value;
                    OnPropertyChanged(nameof(CurrentSolved));
                }
            }
        }
        public int CurrentIssueID
        {
            get => _currentIssueID; set
            {
                if (_currentIssueID != value)
                {
                    _currentIssueID = value;
                    OnPropertyChanged(nameof(CurrentIssueID));
                }
            }
        }

        //Filtering Propertys
        public bool ShowOnlySolved
        {
            get => _showOnlySolved;
            //Filtering Propertys
            set
            {
                if (_showOnlySolved != value)
                {
                    _showOnlySolved = value;
                    FilterIssues();
                    OnPropertyChanged(nameof(ShowOnlySolved));
                }
            }
        }
        public bool SortByDescription
        {
            get => sortByDescription;
            set
            {
                sortByDescription = value;
                FilterIssues();
                OnPropertyChanged(nameof(SortByDescription));
            }

        }



        //Commands
        public ICommand CreateNewIssueCommand { get; private set; }
        public ICommand SaveIssueCommand { get; private set; }

        public bool AddingNewIssue
        {
            get => _addingNewIssue;
            set
            {
                if (_addingNewIssue != value)
                {
                    _addingNewIssue = value;
                    OnPropertyChanged(nameof(AddingNewIssue));
                }
            }
        }
        public IList<IssueModel> AllIssuesList
        {
            get => _allIssuesList; set
            {
                if (_allIssuesList != value)
                {
                    _allIssuesList = value;
                    OnPropertyChanged(nameof(AllIssuesList));
                }
            }
        }
        public IssueModel SelectedIssue
        {
            get => _selectedIssue; set
            {
                if (_selectedIssue != value)
                {
                    _selectedIssue = value;
                    LoadIssueToForm(SelectedIssue);
                    OnPropertyChanged(nameof(SelectedIssue));
                }
            }
        }



        public MainViewModel()
        {
            EnableAddIssueMode();
            CreateNewIssueCommand = new RelayCommand(p => this.EnableAddIssueMode(), p => true);
            SaveIssueCommand = new RelayCommand(p => this.SaveOrUppdateIssue(), p => true);
            AllIssuesList = GetAllIssuesFromDatabase();
            int i = 0;
        }

        public void EnableAddIssueMode()
        {
            AddingNewIssue = true;
            ResetIssueForm();
        }

        public void ResetIssueForm()
        {
            CurrentShortDescription = "";
            CurrentLongDescription = "";
            CurrentErrorCode = "";
            CurrentIssueDate = DateTime.Now;
            CurrentSolved = false;
            CurrentIssueID = -1;
        }

        public void LoadIssueToForm(IssueModel issue)
        {
            AddingNewIssue = false;

            CurrentShortDescription = issue.ShortDescription;
            CurrentLongDescription = issue.LongDescription;
            CurrentErrorCode = issue.ErrorCode;
            CurrentIssueDate = issue.IssueDate;
            CurrentSolved = issue.Solved;
        }

        private void FilterIssues()
        {
            AllIssuesList = GetFilteredIssuesFromDatabase();
        }

        public void SaveOrUppdateIssue()
        {
            //Sparar ett nytt issue
            if (AddingNewIssue)
            {
                IssueModel issue = new IssueModel
                {
                    IssueDate = CurrentIssueDate,
                    ShortDescription = CurrentShortDescription,
                    LongDescription = CurrentLongDescription,
                    Solved = CurrentSolved,
                    ErrorCode = CurrentErrorCode
                };

                AddNewIssueToDatabase(issue);
            }
            else //uppdaterar ett issue
            {
                SelectedIssue.ShortDescription = CurrentShortDescription;
                SelectedIssue.LongDescription = CurrentLongDescription;
                SelectedIssue.ErrorCode = CurrentErrorCode;
                SelectedIssue.IssueDate = CurrentIssueDate;
                SelectedIssue.Solved = CurrentSolved;

                UppdateIssueInDatabase(SelectedIssue);
            }
        }


        public void AddNewIssueToDatabase(IssueModel Issue)
        {
            using (var db = new LiteDatabase(@"IssueDatabase.db"))
            {
                var Issues = db.GetCollection<IssueModel>("Issues");

                Issues.Insert(Issue);
                IndexIssues(Issues);
            }

            //Hämtar alla issues på nytt
            AllIssuesList = GetAllIssuesFromDatabase();
        }

        public void UppdateIssueInDatabase(IssueModel issue)
        {
            using (var db = new LiteDatabase(@"IssueDatabase.db"))
            {
                var issueCollection = db.GetCollection<IssueModel>("issues");

                issueCollection.Update(issue);
            }
        }

        private IList<IssueModel> GetFilteredIssuesFromDatabase()
        {
            var issuesToReturn = new List<IssueModel>();
            using (var db = new LiteDatabase(@"IssueDatabase.db"))
            {
                var issues = db.GetCollection<IssueModel>("issues");

                IEnumerable<IssueModel> results;

                if (ShowOnlySolved && SortByDescription)
                {
                    results = issues
                        .Find(x => x.Solved)
                        .OrderBy(x => x.ShortDescription);
                }
                else if(ShowOnlySolved && !sortByDescription)
                {
                    results = issues
                        .Find(x => x.Solved);
                } 
                else if (!ShowOnlySolved && SortByDescription)
                {
                    results = issues
                        .FindAll()
                        .OrderBy(x => x.ShortDescription);
                }
                else
                {
                    results = issues
                        .FindAll();
                }

                foreach (IssueModel issueItem in results)
                {
                    issuesToReturn.Add(issueItem);
                }
                return issuesToReturn;
            }
        }


        public IList<IssueModel> GetAllIssuesFromDatabase()
        {
            var issuesToReturn = new List<IssueModel>();
            using (var db = new LiteDatabase(@"IssueDatabase.db"))
            {
                var issues = db.GetCollection<IssueModel>("issues");

                var results = issues.FindAll();
                foreach (IssueModel issueItem in results)
                {
                    issuesToReturn.Add(issueItem);
                }
                return issuesToReturn;
            }
        }

        public void IndexIssues(LiteCollection<IssueModel> Issues)
        {
            Issues.EnsureIndex(x => x.IssueID);
            Issues.EnsureIndex(x => x.IssueDate);
            Issues.EnsureIndex(x => x.ShortDescription);
            Issues.EnsureIndex(x => x.LongDescription);
            Issues.EnsureIndex(x => x.Solved);
            Issues.EnsureIndex(x => x.ErrorCode);
        }

    }
}
