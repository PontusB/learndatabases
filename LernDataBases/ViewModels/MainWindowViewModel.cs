﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LernDataBases.ViewModels
{
    class MainWindowViewModel : BaseViewModel
    {
        public BaseViewModel CurrentViewModel { get; set; }
        public MainWindowViewModel()
        {
            CurrentViewModel = new MainViewModel();
        }
    }
}
